# presentcollab

# Does not work on web because of a dependency not yet supporting web.

## What is it?
presentcollab is an application to help collaboratively presenting PDF files.
One person can start a new presentation and give the information to anybody who will be watching and all the presenters. The presenters can then go to any page in the PDF which will be automatically synchronized to anyone watching the Presentation.

## How to use:
The finnished version will run in the browser which will not require installation. This will not work until `firebase_database` supports the web platform. Until then manually building and installing is the only way to use this application.