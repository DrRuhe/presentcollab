import 'package:flutter/material.dart';
import 'package:native_pdf_view/native_pdf_view.dart';
import 'package:presentcollab/state/db.dart';
import 'package:presentcollab/widgets/page_setter.dart';

import 'error_page.dart';
import 'ez_future_builder.dart';

class HostPage extends StatefulWidget {
  final HostingDB db;
  const HostPage({Key? key, required this.db}) : super(key: key);

  @override
  State<HostPage> createState() => _HostPageState();
}

class _HostPageState extends State<HostPage> {
  //this bool exists, because the PDFController can cause errors when used before the onDocumentLoaded callback is completed.
  bool documentInitialized = false;
  @override
  Widget build(BuildContext context) {
    return EzFutureBuilder<PdfController?>(
        future: widget.db.pdfController,
        builder: (pdfController) {
          if (pdfController == null) {
            return ErrorPage(
              msg: 'pdf controller was null',
            );
          }

          return Scaffold(
            body: IgnorePointer(
              child: PdfView(
                controller: pdfController,
                onDocumentLoaded: (_) {
                  setState(() {
                    documentInitialized = true;
                  });
                },
                pageLoader: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            ),
            bottomNavigationBar: documentInitialized
                ? PageSetter(db: widget.db, pdfController: pdfController)
                : null,
          );
        });
  }
}
