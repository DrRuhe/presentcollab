import 'dart:io';
import 'dart:typed_data';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:presentcollab/state/db.dart';
import 'package:prompt_dialog/prompt_dialog.dart';

class HomePage extends StatelessWidget {
  /// A callback for uploading a presentation. Takes the id of the PDF and the hostKey
  final void Function(String, String, DB) createNewPresentation;
  final void Function(String) joinPresentation;
  const HomePage(
      {Key? key,
      required this.createNewPresentation,
      required this.joinPresentation})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min,
      children: [
        Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Text("presentcollab"),
              Text("collaboratively present a pdf online."),
              Center(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                        onPressed: () async {
                          String? input = await prompt(
                            context,
                            title: Text('Id to the Presentation'),
                            textOK: Text('join'),
                            textCancel: Text('cancel'),
                            hintText:
                                'the id can be provided by someone hosting the presentation',
                          );
                          if (input != null) {
                            joinPresentation(input);
                          }
                        },
                        child: Text('join a presentation'),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                        onPressed: uploadPDF,
                        child: Text('start a presentation'),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        TextButton(
            onPressed: () {
              showAboutDialog(
                context: context,
                applicationName: 'PresentCollab'

              );
            },
            child: Text('About'))
      ],
    );
  }

  void askForIdToJoinPresentation() async {}

  void uploadPDF() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowedExtensions: ['pdf'],
        allowMultiple: false);

    if (result != null) {
      File file = File(result.files.first.path!);

      Uint8List data = file.readAsBytesSync();

      String pdfID = (file.path.hashCode * data.hashCode).hashCode.toString();

      String hostKey =
          (data.hashCode * file.lengthSync().hashCode).hashCode.toString();


      DB db = DB.create(pdfID, hostKey, data);

      createNewPresentation(pdfID, hostKey, db);
    } else {
      // User canceled the picker
    }
  }
}
