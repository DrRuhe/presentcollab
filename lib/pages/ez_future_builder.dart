import 'package:flutter/material.dart';

import 'loading_page.dart';

class EzFutureBuilder<T> extends StatelessWidget {
  final Future<T> future;
  final Widget Function(T) builder;

  const EzFutureBuilder({Key? key, required this.future, required this.builder})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<T>(
      future: future,
      builder: (ctx, snapshot) {
        T? data = snapshot.data;
        if (snapshot.hasData && data != null) {
          return builder(data);
        }
        return LoadingPage();
      },
    );
  }
}
