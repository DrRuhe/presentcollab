import 'package:flutter/material.dart';

class ErrorPage extends StatelessWidget {
  final String msg;
  const ErrorPage({Key? key,this.msg = 'an error occurred'}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(child: Center(child: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text('Oops, something happened!'),
        Text(msg),
      ],
    ),),);
  }
}
