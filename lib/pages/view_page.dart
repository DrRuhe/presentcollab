import 'package:flutter/material.dart';
import 'package:native_pdf_view/native_pdf_view.dart';
import 'package:presentcollab/state/db.dart';

import 'error_page.dart';
import 'ez_future_builder.dart';

class ViewPage extends StatelessWidget {
  final DB db;
  const ViewPage({Key? key, required this.db}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return EzFutureBuilder<PdfController?>(
        future: db.pdfController,
        builder: (pdfController) {
          if (pdfController == null) {
            return ErrorPage(
              msg: 'pdf controller was null',
            );
          }

          return Scaffold(
            body: IgnorePointer(
              child: PdfView(
                controller: pdfController,
                pageLoader: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            ),
          );
        });
  }
}