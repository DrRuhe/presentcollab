import 'package:flutter/material.dart';

class LoadingPage extends StatelessWidget {
  final String msg;
  const LoadingPage({Key? key, this.msg = 'an error occurred'}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: CircularProgressIndicator()
      ),
    );
  }
}
