import 'dart:typed_data';

import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:native_pdf_view/native_pdf_view.dart';

class API {
  static const verbose = false;

  final FirebaseStorage _datastore;
  final DatabaseReference _realtimeDB;

  API()
      : _datastore = FirebaseStorage.instance,
        _realtimeDB = FirebaseDatabase.instance.reference();

  /// Get the Value stored in the realtimeDB at the path location.
  /// return value if the received value is of Type T, otherwise (even in case of error getting Data) return null
  Future<T?> getRealtimeData<T>(String path) {
    if (verbose) print('getRealtimeData<T:$T>(path:$path)');
    return _realtimeDB.child(path).get().then((hostKeySnapshot) {
      var value = hostKeySnapshot?.value;
      if (value is T) {
        return value;
      } else {
        return null;
      }
    }, onError: (error, stackTrace) {
      print(error);
      return null;
    });
  }

  ///get a Stream of Type T corresponding to changes in the RealtimeDB.
  ///Will skip all values that cannot be cast into a T.
  Stream<T> subscribeRealtimeData<T>(String path) {
    if (verbose) print('subscribeRealtimeData<T:$T>(path:$path)');
    return _realtimeDB
        .child(path)
        .onValue
        .map((element) {
          if (verbose) {
            print(
                'Subscription of path:$path (<T:$T>) triggered: ${element.snapshot.value}');
          }
          return element;
        })
        .skipWhile((element) => element.snapshot.value is! T)
        .map((event) => event.snapshot.value as T);
  }

  Future<void> setRealtimeData(String path, dynamic value) {
    if (verbose) print('setrealtimeData(path:$path,value:$value)');
    return _realtimeDB.child(path).set(value);
  }

  Future<Uint8List?> getDatastore(String path) {
    if (verbose) print('getDatastore(path:$path)');

    return _datastore.ref(path).getData().onError((error, stackTrace) {
      print(error);
      return null;
    });
  }

  UploadTask setDatastore(String path, Uint8List data) {
    if (verbose) print('setDatastore(path:$path,data:$data)');
    return _datastore.ref(path).putData(data);
  }
}

class DB {
  static const String _presentations = 'presentations';
  static const String _pagenumber = 'page';

  final API api = API();
  final String pdfID;

  late final String _pdfLocation = 'uploads/$pdfID.pdf';
  late final String _pageLocation = '$_presentations/$pdfID/$_pagenumber';
  late final Future<PdfController?> pdfController = _pdfDataGetter.then(
    (data) {
      print('download has finnished!');

      if (data == null) return null;
      var pdfController = PdfController(document: PdfDocument.openData(data));
      //everytime a new pagenumber gets transmitted, jump to that page and update the current page
      pages.forEach(
        (page) {
          try {
            pdfController.jumpToPage(page);
          } catch (e) {
            print(e);
          }
          _currentPage = page;
        },
      );
      return pdfController;
    },
  );
  late final Future<int?> pagesCount =
      pdfController.then((value) => value!.pagesCount);
  late final Stream<int> pages = api.subscribeRealtimeData<int>(_pageLocation);

  int _currentPage = 1;

  late Future<Uint8List?> _pdfDataGetter = api.getDatastore(_pdfLocation);

  DB.view(this.pdfID);
  factory DB.host(String pdfID, String hostID) => HostingDB(pdfID, hostID);
  factory DB.create(String pdfID, String hostID, Uint8List data) =>
      HostingDB.create(pdfID, hostID, data);

  int get currentPage => _currentPage;
}

class HostingDB extends DB {
  static const String _hostkey = 'hostkey';

  final String hostKey;

  late final String _hostKeyLocation = '${DB._presentations}/$pdfID/$_hostkey';
  late final bool validHostKey;

  HostingDB(String pdfID, this.hostKey) : super.view(pdfID);
  HostingDB.create(String pdfID, this.hostKey, Uint8List data)
      : super.view(pdfID) {
    _pdfDataGetter = Future.value(data);
    //Upload PDF
    api.setDatastore(_pdfLocation, data);
    //set host key
    api.setRealtimeData(_hostKeyLocation, hostKey);
    //set the page to 1
    setPage(1);
  }

  set currentPage(int value) {
    print('setter was called');
    setPage(value);
  }

  Future<void> setPage(int page) {
    if (credentialsAreValid()) {
      return api.setRealtimeData(_pageLocation, page);
    }
    return Future(() => null);
  }

  bool credentialsAreValid() {
    //check if there is an entry for a pdf with the corresponding ID and the hostkey
    return true;
  }
}
