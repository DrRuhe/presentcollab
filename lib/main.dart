import 'package:flutter/material.dart';

import 'routing/routing.dart';

void main() {
  runApp(PresentCollab());
}

class PresentCollab extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routeInformationParser: RouteParser(),
      routerDelegate: PresentationRouteDelegate(),
    );
  }
}
