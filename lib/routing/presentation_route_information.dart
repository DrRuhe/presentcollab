import 'package:presentcollab/state/db.dart';

class PresentationRouteInformation {
  final String? pdfID;
  final String? hostKey;
  late final DB? db;

  PresentationRouteInformation.home()
      : pdfID = null,
        hostKey = null,
        db = null;

  PresentationRouteInformation.view(String this.pdfID, {DB? providedDB})
      : hostKey = null,
        db = providedDB ?? DB.view(pdfID);

  PresentationRouteInformation.host(String this.pdfID,String this.hostKey,
      {DB? providedDB})
      : db = providedDB ?? DB.host(pdfID, hostKey);

  bool get isHomePage => pdfID == null;
  bool get isViewPage => pdfID != null && hostKey == null;
  bool get isHostPage => pdfID != null && hostKey != null && db is HostingDB;
}
