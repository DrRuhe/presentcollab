import 'package:flutter/material.dart';
import 'package:presentcollab/pages/pages.dart';
import 'package:presentcollab/state/db.dart';
import 'package:presentcollab/widgets/project_frame.dart';
import 'routing.dart';

class PresentationRouteDelegate
    extends RouterDelegate<PresentationRouteInformation>
    with
        ChangeNotifier,
        PopNavigatorRouterDelegateMixin<PresentationRouteInformation> {
  PresentationRouteInformation routeInfo = PresentationRouteInformation.home();

  @override
  PresentationRouteInformation? get currentConfiguration => routeInfo;

  @override
  GlobalKey<NavigatorState> get navigatorKey => GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return ProjectFrame(
      child: Navigator(
        key: navigatorKey,
        pages: [
          MaterialPage(
              child: HomePage(
            createNewPresentation: (pdfID, hostKey, db) {
              routeInfo = PresentationRouteInformation.host(pdfID, hostKey,
                  providedDB: db);
              notifyListeners();
            },
            joinPresentation: (pdfID) {
              routeInfo = PresentationRouteInformation.view(pdfID);
              notifyListeners();
            },
          )),
          if (routeInfo.isViewPage)
            MaterialPage(child: ViewPage(db: routeInfo.db!)),
          if (routeInfo.isHostPage)
            MaterialPage(child: HostPage(db: routeInfo.db! as HostingDB)),
        ],
        onPopPage: (route, result) {
          if (!route.didPop(result)) return false;
          return true;
        },
      ),
    );
  }

  @override
  Future<void> setNewRoutePath(
      PresentationRouteInformation configuration) async {
    routeInfo = configuration;
  }
}
