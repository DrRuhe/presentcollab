export './presentation_route_delegate.dart';
export './presentation_route_information.dart';
export './route_parser.dart';

class RoutesDeclaraions {
  static const String home = "";
  static const String view = "view";
  static const String host = "present";

  static const String homeLink = "/";
  static const String viewLink = "/$view";
  static const String hostLink = "/$host";
}
