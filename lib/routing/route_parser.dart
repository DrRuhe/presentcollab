
import 'package:flutter/material.dart';
import 'package:presentcollab/routing/routing.dart';

class RouteParser extends RouteInformationParser<PresentationRouteInformation> {
  @override
  Future<PresentationRouteInformation> parseRouteInformation(
      RouteInformation routeInformation) async {
    var location = routeInformation.location ?? RoutesDeclaraions.homeLink;
    var uri = Uri.parse(location);

    var pathSegments = uri.pathSegments;
    switch (pathSegments.length) {
      case 2:
        if (pathSegments.first != RoutesDeclaraions.view) break;
        return PresentationRouteInformation.view(pathSegments[1]);
      case 3:
        if (pathSegments.first != RoutesDeclaraions.host) break;
        return PresentationRouteInformation.host(
            pathSegments[1], pathSegments[2]);
    }
    return PresentationRouteInformation.home();
  }

  @override
  RouteInformation? restoreRouteInformation(
      PresentationRouteInformation configuration) {
    String location;
    if (configuration.isViewPage) {
      location = "/${RoutesDeclaraions.view}/${configuration.pdfID}";
    } else if (configuration.isHostPage) {
      location =
          "/${RoutesDeclaraions.host}/${configuration.pdfID}/${configuration.hostKey}";
    } else {
      //unknown and home page
      location = RoutesDeclaraions.homeLink;
    }
    return RouteInformation(location: location);
  }
}
