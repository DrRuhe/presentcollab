import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:presentcollab/pages/error_page.dart';
import 'package:presentcollab/pages/loading_page.dart';

class ProjectFrame extends StatelessWidget {
  final Widget? child;
  const ProjectFrame({Key? key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Initialize FlutterFire
      future: Firebase.initializeApp(),
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          return ErrorPage();
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          return Scaffold(
            body: child,
          );
        }

        // Otherwise, show something whilst waiting for initialization to complete
        return LoadingPage();
      },
    );



    // return Provider<AppState>(
    //   create: (_) => AppState(),
    //   child: Scaffold(
    //     body: child,
    //   ),
    // );
  }
}
