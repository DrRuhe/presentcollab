import 'package:flutter/material.dart';
import 'package:native_pdf_view/native_pdf_view.dart';
import 'package:presentcollab/pages/loading_page.dart';
import 'package:presentcollab/state/db.dart';

class PageSetter extends StatelessWidget {
  final HostingDB db;
  final PdfController pdfController;

  const PageSetter({Key? key, required this.db, required this.pdfController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<int>(
      stream: db.pages,
      initialData: db.currentPage,
      builder: (context, snapshot) {
        if (!snapshot.hasData || snapshot.data == null) {
          return Container(height: 10, child: LoadingPage());
        }

        int currentPage = snapshot.data!;
        // print('pageSetter with page $currentPage. db reports ${db.currentPage} as current page, the pagesCount is ${pdfController.pagesCount}');

        bool isOnFirstPage = currentPage == 1;
        bool isOnLastPage = currentPage == pdfController.pagesCount;

        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Slider(
              value: pdfController.page.toDouble(),
              divisions: pdfController.pagesCount - 1,
              min: 1,
              max: pdfController.pagesCount.toDouble(),
              onChanged: (value) {
                db.currentPage = value.round();
              },
            ),
            ButtonBar(
              alignment: MainAxisAlignment.center,
              children: [
                MaterialButton(
                  onPressed: gotoFirstPage,
                  child: Icon(Icons.first_page),
                ),
                MaterialButton(
                  onPressed: isOnFirstPage ? null : gotoPreviousPage,
                  child: Icon(Icons.navigate_before),
                ),
                Row(
                  children: [
                    Text('${pdfController.page}/${pdfController.pagesCount}'),
                  ],
                ),
                MaterialButton(
                  onPressed: isOnLastPage ? null : gotoNextPage,
                  child: Icon(Icons.navigate_next),
                ),
                MaterialButton(
                  onPressed: gotoLastPage,
                  child: Icon(Icons.last_page),
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  void gotoFirstPage() {
    db.currentPage = 1;
  }

  void gotoLastPage() {
    db.currentPage = pdfController.pagesCount;
  }

  void gotoNextPage() {
    db.currentPage++;
  }

  void gotoPreviousPage() {
    db.currentPage--;
  }
}
