import 'package:flutter/material.dart';
import 'package:native_pdf_view/native_pdf_view.dart';
import 'package:presentcollab/pages/error_page.dart';
import 'package:presentcollab/pages/ez_future_builder.dart';

class MyPDFView extends StatelessWidget {
  final Future<PdfController?> pdfController;
  const MyPDFView({Key? key, required this.pdfController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return EzFutureBuilder<PdfController?>(
        future: pdfController,
        builder: (pdfController) {
          if (pdfController == null) {
            return ErrorPage(
              msg: 'pdf controller was null',
            );
          }
          return PdfView(controller: pdfController);
        });
  }
}
